# cycIF_int_norm

Robust Intensity Normalization Method for Multiplexed Imaging ([bioRxiv](https://www.biorxiv.org/content/10.1101/792770v1))

Contact: Young Hwan Chang (chanyo@ohsu.edu)


Our approach is composed of two parts: 1) identify mutually exclusive marker pairs (i.e., negative control markers) in multiplexed imaging data and 2) normalize reference intensity by the inferred autofluorescence level of negative control.
We use Efficient Sparse Subspace Clustering by Nearest Neighbour Filtering (kssc) for identifying cell types (required libraries: kssc, Subkit, NCut, Vlfeat). In order to test our code (MATLAB R2017b), it is necesary to install/add libraries as below: 
*  https://arxiv.org/abs/1704.03958
*  https://github.com/sjtrny/kssc
*  Subkit: https://github.com/sjtrny/SubKit
*  NCut: http://www.cis.upenn.edu/~jshi/software/
*  Vlfeat: http://www.vlfeat.org/


We test our methods with two different datasets :

1.  59 TMAs with three adjacent sections:
To run analysis code, you need to download data file from [Link](https://www.dropbox.com/sh/yai5p0x8h1iujhj/AADZxBArryLj-0VvxS4D8LDQa?dl=0):

[input data file]
*  run_BM-Her2N75-15_MeanIntensity_filtered.csv
*  run_BM-Her2N75-17_MeanIntensity_filtered.csv
*  run_BM-Her2N75-18_MeanIntensity_filtered.csv


[output data file by running gate_adj_TMA_final.m] (with varying N_group = 5,10,15,20)
*  thresh_result_data.mat
*  est_result_data_Ngroup_5.mat
*  est_result_data_Ngroup_10.mat
*  est_result_data_Ngroup_15.mat
*  est_result_data_Ngroup_20.mat  


2.  longitudinal biopsies sample (Bx1/Bx2): 
to run this code ( ex_int_norm_long_bx.m), you need to download data file from [Link](https://www.dropbox.com/sh/8vqqpo081cj1i6t/AACzIB2StfxEZKmD-bknnzISa?dl=0):
*  Feat_Bx1.csv
*  Feat_Bx2.csv



