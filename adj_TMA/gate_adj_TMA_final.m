
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% author: Young Hwan Chang (chanyo@ohsu.edu)
% https://sites.google.com/site/yhchangucb
% This code is implemented in MATLAB R2017b

% To run this code, you need to download data file
% * datafile can be downloaded from Dropbox link: https://www.dropbox.com/sh/yai5p0x8h1iujhj/AADZxBArryLj-0VvxS4D8LDQa?dl=0
% * put datafile under ./data/: 
%     ./data/run_BM-Her2N75-15_MeanIntensity_filtered.csv
%     ./data/run_BM-Her2N75-17_MeanIntensity_filtered.csv
%     ./data/run_BM-Her2N75-18_MeanIntensity_filtered.csv


clear all
clc
close all

rng('default');

output = './result/';
mkdir(output);

FLAG_SSC  = 1; % for clustering pos/neg group, we use Sparse Subspace Clustering. if FLAG_SSC = 0, we use kmeans clustering as an alternate approach.
FLAG_PLOT = 1;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Meta table for mutually exclusive marker pairs
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% reference maker
marker_pairs_ref = {'CD4', 'Ecad', 'PD1', 'CK7', 'GRNZB', ...
                    'CK17', 'PDPN', 'CK19', 'Her2', 'FoxP3', ...
                    'PgR', 'ER', 'CD3', ...
                    'CD45', 'aSMA', 'CD68', ...
                    'Vim', 'CK14', 'CK5', 'CD8', 'CD31', 'CD20', ...
                    'EGFR', 'CK8', 'FoxP3bio', 'ColI', 'ColIV', 'PCNA', 'Ki67' , 'CD44', ...
                    };
                
% corresponding target marker (i.e., mutually exclusive marker pairs)
marker_pairs_tgr = {'CK19', 'CD68', 'CK19', 'CD68', 'CK19', ...
                    'CD31', 'CK19', 'CD68', 'CD68', 'CK19', ...
                    'CD68', 'CD68', 'CK19', ...
                    'CK19', 'CK19', 'CK19', ...
                    'CK19', 'CD68', 'CD68', 'CK19', 'CK19', 'CK19', ...
                    'CD31', 'CD68', 'Ck19', 'CK19', 'CK19', 'CD68', 'CD68', 'CK19', ...
                    };

% selected scene in TMAs
unique_scene = {'scene001', 'scene005', 'scene006' ... 
                'scene007', 'scene012', 'scene013', ...
                'scene014', 'scene015', 'scene020', ...
                'scene021', 'scene033', 'scene034', ...
                'scene035', 'scene036', 'scene037', ...
                'scene038', 'scene044', 'scene045', ...
                'scene046', 'scene047', 'scene048', ...
                'scene049', 'scene062', 'scene063', ...
                'scene064', 'scene065', 'scene066', ...
                'scene067', 'scene068', 'scene073', ...
                'scene074', 'scene075', 'scene076', ...
                'scene077', 'scene089', 'scene090', ...
                'scene092', 'scene093', 'scene095', ...
                'scene098', 'scene100', 'scene101', ...
                'scene102', 'scene103', 'scene104', ...
                'scene108', 'scene109', 'scene110', ...
                'scene111', 'scene112', 'scene117', ...
                'scene119', 'scene123', 'scene127', ...
                'scene128', 'scene129', 'scene130', ...
                'scene135', 'scene152'};
                
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% data loading

data{1} = readtable('./data/run_BM-Her2N75-15_MeanIntensity_filtered.csv');
data{2} = readtable('./data/run_BM-Her2N75-17_MeanIntensity_filtered.csv');
data{3} = readtable('./data/run_BM-Her2N75-18_MeanIntensity_filtered.csv');


marker_name_org = data{1}.Properties.VariableNames(2:end);
for i=1:length(marker_name_org)
    ii = strfind(marker_name_org{i}, '_');
    if isempty(ii) == 1
        marker_name{i} = marker_name_org{i};
    else
        marker_name{i} = marker_name_org{i}(1:ii(1)-1);
    end
end


for batch = 1:length(data)
    data{batch}.mInt = table2array(data{batch}(:,2:end));
end





%% index for paired markers:

marker_pairs = [];
for i=1:length(marker_name)
    id_ref = []; id_ref = find(strcmp(marker_pairs_ref, marker_name{i})==1);
    if isempty(id_ref) ~= 1
        id_tgr = [];
        id_tgr = find(strcmp(marker_name, marker_pairs_tgr{id_ref}));
        marker_pairs = [marker_pairs; [i id_tgr]];
    end
    
end


%% clustering
% for practical purpose, we select data region by 1) removing low intensity
% cells by using cutoff "th_mInt" and only including subset of dataset (ratio_x, ratio_y)
th_mInt = 50; % cutoff for intensity 
ratio_x = 0.75; % quantile for x
ratio_y = 0.5; % quantile for y

for scene = 1:length(unique_scene)

   
  
    fprintf(sprintf('%s--------------------------\n', unique_scene{scene}));
   
    if FLAG_PLOT == 1
        close all;
        figure('pos', [10 10 1000 1400]);
    end


    mIntData = []; L_smpl = [];
    
    % data loading
    N_total = [];
    for batch=1:length(data)      
        % selected data region
        id = []; id = find(strncmp(data{batch}.Var1, unique_scene{scene}, length(unique_scene{scene} )) == 1);
        mIntData = [mIntData; data{batch}.mInt(id,:)];
        L_smpl = [L_smpl; ones(length(id),1)*batch];
        N_total(batch) = length(id);
    end
    
    NN_comp = []; Thresh_all = [];
    for i=1:size(marker_pairs,1)
        N_cluster = 2; % simply partition into two group 
        
        id_cell = find( (mIntData(:, marker_pairs(i,2)) > th_mInt  & ...
                            mIntData(:, marker_pairs(i,1)) > quantile( mIntData(:, marker_pairs(i,1)), ratio_x) ) | ...
                        (mIntData(:, marker_pairs(i,1)) > th_mInt) & ...
                            mIntData(:, marker_pairs(i,2)) > quantile( mIntData(:, marker_pairs(i,2)), ratio_y));
        Feat = []; 
        Feat = [mIntData(:, marker_pairs(i,1)) mIntData(:, marker_pairs(i,2))];
        
        if FLAG_SSC == 1 % if we use sparse subspace clustering
            Z_kssc = kssc_exact_par(Feat', 0.1, 500); % parameter chosen based on reasonable performance
            [kssc_clusters,NcutEigenvectors,NcutEigenvalues] = ncutW((abs(Z_kssc)+abs(Z_kssc')), N_cluster);

            L = condense_clusters(kssc_clusters,1);
        else % if we use k-means clustering 
            L = litekmeans(Feat, N_cluster);
        end
        
        % from two cluster, we automatically identify pos cell group vs negative group
        % for infering background signal
        mean_value = [mean( Feat(find(L==1), 1)) mean(Feat(find(L==2),1))];

        if mean_value(1) > mean_value(2)
            flag_neg = 2;
        else
            flag_neg = 1; 
        end
        
        %%% global est
        % we identify background signal (or autofluorescence signal) from
        % all three TMA adjacent sections (we refer this "global")
        % we choose max value of (mean + 3STD, 0.95 quantile in negative group)
        thresh_global_est = max(mean(Feat( find(L==flag_neg),1)) + 3*std(Feat( find(L==flag_neg),1)), ...
                        quantile(Feat( find(L==flag_neg), 1), 0.95))*1.10;

                    
        %%% local estimate
        % we identify background signal (or autofluorescence signal) from
        % individual TMA section (we refer this "local")
        
        Thresh_Est = zeros(length(data), 2);
        N_comp = [];
        for batch=1:length(data)
            id = []; id = find(L_smpl == batch);
            
            if FLAG_PLOT == 1           
                subplot( size(marker_pairs,1)/2, 6, (i-1)*3+batch )

                plot( mIntData( :, marker_pairs(i,1)), mIntData (:, marker_pairs(i,2)),'.', 'color', [211/255 211/255 221/255]);
                hold on;
                plot( mIntData( id, marker_pairs(i,1)), mIntData (id, marker_pairs(i,2)),'.r')
            end
            
            
            % quantile
            id_select = [];
            id_select = find ( (( mIntData(id, marker_pairs(i,2)) > th_mInt ) & ...
                                mIntData(id, marker_pairs(i,1)) > quantile( mIntData(id, marker_pairs(i,1)), ratio_x)) | ...
                               (( mIntData(id, marker_pairs(i,1)) > th_mInt) & ...
                               mIntData(id, marker_pairs(i,2)) > quantile( mIntData(id, marker_pairs(i,2)), ratio_y) ));

            Feat = [  mIntData(id(id_select), marker_pairs(i,1)) mIntData(id(id_select), marker_pairs(i,2))];
            

            if FLAG_SSC == 1 % if we use sparse subspace clustering
                Z_kssc = kssc_exact_par(Feat', 0.1, 500);
                [kssc_clusters,NcutEigenvectors,NcutEigenvalues] = ncutW((abs(Z_kssc)+abs(Z_kssc')), N_cluster);

                L = condense_clusters(kssc_clusters,1);
            else % if we use k-means clustering 
                L = litekmeans(Feat, N_cluster);
            end

            mean_value = [mean( Feat(find(L==1), 1)) mean(Feat(find(L==2),1))];

            if mean_value(1) > mean_value(2)
                flag_neg = 2;
            else
                flag_neg = 1; 
            end

             % we choose max value of (mean + 3STD, 0.95 quantile in negative group)
            thresh_est = (max(mean(Feat( find(L==flag_neg),1)) + 3*std(Feat( find(L==flag_neg),1)), ...
                            quantile(Feat( find(L==flag_neg), 1), 0.95))*1.10); 

            if FLAG_PLOT == 1        

                         plot([ thresh_global_est thresh_global_est], ...
                                    [0 max(mIntData(:, marker_pairs(i,2)))], 'k-','LineWidth',2.5);

                         plot([ thresh_est thresh_est], ...
                                    [0 max(mIntData(:, marker_pairs(i,2)))], 'g--','LineWidth',2.0);


                        xlabel( marker_name {marker_pairs(i,1)});
                        ylabel( marker_name {marker_pairs(i,2)});
                        axis([-100 max(quantile(mIntData(:,marker_pairs(i,1)),0.99))*2.0 ...
                                -100 max(quantile(mIntData(:,marker_pairs(i,2)),0.99))*2.0]);


                        title(sprintf('batch %d', batch));
            end
            Thresh_Est(batch,:)  = [thresh_est thresh_global_est];
            
            % local N, global N, total N
            % measure pos/neg cell population based on local est vs global
            % est
            N_comp = [ N_comp  length(find( mIntData(id, marker_pairs(i,1)) > thresh_est)) ...
                                length(find( mIntData(id, marker_pairs(i,1)) > thresh_global_est)) ...
                                ];
            
        end
        Thresh_all = [Thresh_all; Thresh_Est(:,1)' Thresh_Est(1,2)'];
        NN_comp = [NN_comp; N_comp];
        
        
        if FLAG_PLOT == 1
                saveas(gcf, sprintf('./%s/%s_gate_dist.png', output, unique_scene{scene}), 'png');
        end
        
    end
    
    %% Cell component calculated by inferred signal 
    NN_final = NN_comp(:, [1 3 5 2 4 6]); % local / global for each adj section
    
    NN_ratio = NN_final;
    for k=1:length(N_total)
        NN_ratio(:, [1 2]+(k-1)*2) = NN_ratio(:, [1 2]+(k-1)*2)./N_total(k);
    end
    
      
    Result{scene}.Cnt = [NN_final; N_total N_total];
    Result{scene}.Th = Thresh_all; % local 1 / local 2 / local 3 / global est
    
    
    %% we also run simple kmeans clustering for identifying cell population
    N_group = 10; % number of group is chosen by 10 here. In the paper, we run kmeans clustering with N_group = 5, 10, 15, 20
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% without intensity normalization
    feat_org = mIntData(:, marker_pairs(:,1));
    L_kmeans_org = litekmeans(zscore(feat_org), N_group, 'MaxIter', 50, 'Replicates', 5);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    feat_norm = []; feat_norm = feat_org;
   
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% with intensity normalization by inferring background signal
    for batch=1:3
        id_smpl = []; id_smpl = find(L_smpl == batch);
        for l=1:size(marker_pairs,1)
            if Thresh_all(l,end) == 0
                feat_norm(id_smpl,l) = feat_norm(id_smpl,l) ./ Thresh_all(l, batch) * mean(Thresh_all(l,1:3));
            else
                feat_norm(id_smpl,l) = feat_norm(id_smpl,l) ./ Thresh_all(l, batch) *Thresh_all(l,end);
            end
        end
    end
        
    L_kmeans_norm = litekmeans(zscore(feat_norm), N_group,  'MaxIter', 50, 'Replicates', 5);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    
    %% cell population comparison based on clustering result w/ and w/o normalization
    Cnt_kmeans = []; Cnt_kmeans_norm = [];
    for batch=1:3
        id_smpl = []; id_smpl = find(L_smpl == batch);
        for group = 1:N_group
            Cnt_kmeans(batch, group) = length(find( L_kmeans_org(id_smpl) == group));
            Cnt_kmeans_norm(batch,group) = length(find(L_kmeans_norm(id_smpl) == group));
        end
    end
    
  
    
    close all
    figure
    subplot(211); bar(Cnt_kmeans'./sum(Cnt_kmeans'), 'grouped'); legend('batch 1', 'batch 2', 'batch 3')
    subplot(212); bar(Cnt_kmeans_norm'./sum(Cnt_kmeans_norm'),'grouped');
    %saveas(gcf, sprintf('./%s/comp_%s.png', output, unique_scene{scene}), 'png');
    saveas(gcf, sprintf('./%s/comp_%s_Ngroup_%d.png', output, unique_scene{scene}, N_group), 'png');

    
    Result{scene}.feat_org = feat_org;
    Result{scene}.feat_norm = feat_norm;
    
    Result{scene}.Cnt_kmeans = Cnt_kmeans;
    Result{scene}.Cnt_kmeans_norm = Cnt_kmeans_norm;
end

save(sprintf('est_result_data_Ngroup_%d.mat', N_group), 'Result');

