%% author: Young Hwan Chang (chanyo@ohsu.edu)
% post analysis code: 
% 1) comparison corr. coeff of cell component by using local and global thresholding approach
% 2) comparison corr. coeff across cycIF markers by using local and global thresholding approach
% 3) comprsion corr. coeff based on unsupervised clustering w/ and w/o
% intenisty normalization

clear all;
clc;
close all;



R{1} = load('./result_paper/est_result_data_Ngroup_5.mat');
R{2} = load('./result_paper/est_result_data_Ngroup_10.mat');
R{3} = load('./result_paper/est_result_data_Ngroup_15.mat');
R{4} = load('./result_paper/est_result_data_Ngroup_20.mat');


marker_info = {'CD20', 'CD44', 'CD45', 'CD4', 'CD68', 'CD8', ...
                'CK14', 'CK19', 'CK5', 'CK7', 'ER', 'Ecad', ...
                'Her2', 'Ki67', 'PCNA', 'PD1', 'Vim', 'aSMA'};
            
unique_scene = {'scene001', 'scene005', 'scene006' ... 
                'scene007', 'scene012', 'scene013', ...
                'scene014', 'scene015', 'scene020', ...
                'scene021', 'scene033', 'scene034', ...
                'scene035', 'scene036', 'scene037', ...
                'scene038', 'scene044', 'scene045', ...
                'scene046', 'scene047', 'scene048', ...
                'scene049', 'scene062', 'scene063', ...
                'scene064', 'scene065', 'scene066', ...
                'scene067', 'scene068', 'scene073', ...
                'scene074', 'scene075', 'scene076', ...
                'scene077', 'scene089', 'scene090', ...
                'scene092', 'scene093', 'scene095', ...
                'scene098', 'scene100', 'scene101', ...
                'scene102', 'scene103', 'scene104', ...
                'scene108', 'scene109', 'scene110', ...
                'scene111', 'scene112', 'scene117', ...
                'scene119', 'scene123', 'scene127', ...
                'scene128', 'scene129', 'scene130', ...
                'scene135', 'scene152'};


%% comparison (local vs global) for 3 adj TMA sections
% corr. coeff. based on cell composition comparison
% by counting pos/neg cell based on the inferred background signal (local vs global)

corr_R = [];
for scene=1:length(unique_scene);
    
        variation_i = []; variation = []';
        mean_i = [];

        N = R{1}.Result{scene}.Cnt(1:end-1,:);
      
        for j=1:length(marker_info)
            comp{j}.local(scene, :) = N(j,1:3);
            comp{j}.global(scene,:) = N(j,4:6);
         end
        
         N = N./R{1}.Result{scene}.Cnt(end,:)*100;

         
      
        r1 = corrcoef(N(:,1:3));
        r2 = corrcoef(N(:,4:6));
        
        corr_R = [corr_R; ...
                    [r1(1,2) r1(1,3) r1(2,3) r2(1,2) r2(1,3) r2(2,3)]];
           % r13 -> r23
           % r23 -> r13 in section so we change plot label below
        
       
end

figure(1)
subplot(121); 
violinplot(corr_R(:,1:3)); title('corr coef (cell comp. based) - local')
xticklabels({'r_{12}', 'r_{23}', 'r_{13}'});
axis([0 4 -1 1]);
grid on;
subplot(122);
violinplot(corr_R(:,4:6)); title('corr coef (cell comp. based) - global')
xticklabels({'r_{12}', 'r_{23}', 'r_{13}'});
axis([0 4 -1 1]);
grid on;


%% comparison (local vs global) for 3 adj TMA sections
% corr. coeff. for individual marker (18 markers)
% by counting pos/neg cell based on the inferred background signal (local vs global)


corr_R1 = [];
corr_R2 = [];
for j=1:length(comp)
    c1 = corrcoef(comp{j}.local);
    c2 = corrcoef(comp{j}.global);
    
    corr_R1 = [corr_R1; c1(1,2) c1(1,3) c1(2,3)];
    corr_R2 = [corr_R2; c2(1,2) c2(1,3) c2(2,3)];
end

figure(2)
subplot(121);
violinplot(corr_R1);
title('corr coef (marker or cell based) - local')
xticklabels({'r_{12}', 'r_{23}', 'r_{13}'});
axis([0 4 -0.25 1])
grid on;
subplot(122);
violinplot(corr_R2);
title('corr coef (marker or cell based) - global')
xticklabels({'r_{12}', 'r_{23}', 'r_{13}'});
axis([0 4 -0.25 1])
grid on;


%% cell composition comparision by using unsupervised clustering w/ and w/o normalization
% corr. coeff. based on cell composition comparison by using kmean clustering w/ and w/o normalization

xtick_label = {'r_{12}','r_{23}', 'r_{13}'};
N_cond = length(R);
for k=1:length(R)
    
    X = []; Y = [];
    for scene = 1:length(unique_scene)
        Cnt_wo = []; Cnt_w = [];
        Cnt_wo = R{k}.Result{scene}.Cnt_kmeans;
        Cnt_w = R{k}.Result{scene}.Cnt_kmeans_norm;

        Ratio_wo = []; Ratio_w = [];
        Ratio_wo = [Cnt_wo'./sum(Cnt_wo')];
        Ratio_w = [Cnt_w'./sum(Cnt_w')];

        cor_wo = corr(Ratio_wo);
        cor_w = corr(Ratio_w);

        x = [cor_wo(1,2) cor_wo(1,3) cor_wo(2,3)];
        y = [cor_w(1,2) cor_w(1,3) cor_w(2,3)];
        
        X = [X; x];
        Y = [Y; y];
        

   
    end
    
    figure(3)
    subplot(2, length(R),k);    violinplot(X); 
    xticklabels(xtick_label); ylabel('corr. coeff'); 
    axis([0 4 -1.5 1.5]); grid on;
    xlabel('batch (i)-(j)');
    title(sprintf('cluster (N=%d) w/o norm.', length(R{k}.Result{1}.Cnt_kmeans)))
    subplot(2, length(R), N_cond+k); violinplot(Y);
    xticklabels(xtick_label); ylabel('corr. coeff');
    title(sprintf('cluster (N=%d) w/ norm.', length(R{k}.Result{1}.Cnt_kmeans)))
    axis([0 4 -1.5 1.5]); grid on;
    xlabel('batch (i)-(j)'); 
end




