%% author: Young Hwan Chang (chanyo@ohsu.edu)
% loading inferred threshold value and pos/neg cell number 
% calculate coefficient of variation

% data file include 
% Result{x}.Cnt
% local 1 2 3 / global 1 2 3
% last row : total cell


clear all;
clc;
close all;



load('./result_paper/thresh_result_data.mat');


marker_info = {'CD20', 'CD44', 'CD45', 'CD4', 'CD68', 'CD8', ...
                'CK14', 'CK19', 'CK5', 'CK7', 'ER', 'Ecad', ...
                'Her2', 'Ki67', 'PCNA', 'PD1', 'Vim', 'aSMA'};
   




%%
close all
for i=1:length(marker_info)
    
    variation_i = []; variation = []';
    mean_i = [];
    for j=1:length(Result)
        N = Result{j}.Cnt(i,:);
        N = N./Result{j}.Cnt(end,:)*100;
        
      
        variation(j,:) = [var(N(:,(1:3))) ...
                            var(N(:,(4:6)))];
        mean_i(j,:) = [mean(N(:, (1:3))) mean(N(:, (4:6)))];
        variation_i(j,:) = [var(N(:,(1:3)))/mean(N(:,(1:3))) ...
                            var(N(:,(4:6)))/mean(N(:,(4:6)))];

    
    end
    
  

    figure(1);
    subplot(4,5,i)
    vplot1 = [variation_i(:,1); variation_i(:,2)];
    vplot2 = [ones(size(variation_i(:,1),1),1);ones(size(variation_i(:,1),1),1)*2];
    violinplot(vplot1, vplot2);
    xticklabels({'local','global'});
    colormap jet
    title(marker_info{i});
    ylabel(' \sigma/\mu');
    xlabel(' \sigma/\mu');
    
  
end