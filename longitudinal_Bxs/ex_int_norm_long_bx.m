%% Author: Young Hwan Chang (chanyo@ohsu.edu)
% cycIF intensity normalization for longitudinal biopsy sample
%

clc;
clear all;
close all;


rng('default');


marker_size = 4;

output_path = './result_paper/';
FLAG_SSC = 1;
FLAG_PLOT  = 0;


FLAG_INFER = 1; % run bg inference modeling



for bx = 1:2
%     
    switch(bx)
        case 1, Bx{bx}.MI = readtable('./Feat_Bx1.csv');
        case 2, Bx{bx}.MI = readtable('./Feat_Bx2.csv');
    end
    Bx{bx}.Feat = table2array(Bx{bx}.MI(:,2:end));
    
    
    Bx{bx}.FeatName = Bx{bx}.MI.Properties.VariableNames(2:end);
    
    for i=1:length(Bx{bx}.FeatName)
        ii = strfind(Bx{bx}.FeatName{i}, '_'); Bx{bx}.FeatName{i}(ii) = '-';
    end

    cell_id = []; scene = [];
    for i=1:size(Bx{bx}.MI,1)
        cell_name = []; ii = strfind(Bx{bx}.MI.UNIQID{i}, '_');
        cell_name = Bx{bx}.MI.UNIQID{i}(ii(1)+1:end);

        scene{i} = Bx{bx}.MI.UNIQID{i}(ii(1)+1:ii(2)-1);


        cell_id = [ cell_id; find(strcmp( Bx{bx}.MI.UNIQID, cell_name) == 1)];
    end

    uniq_scene = [];
    uniq_scene = unique(scene);
    Bx{bx}.uniq_scene = uniq_scene;
    scene_id = [];
    for i=1:length(scene)
        scene_id = [ scene_id; find(strcmp( scene{i}, uniq_scene)==1)];
    end


end



Xn = []; Feat = []; L_BX = []; Feat_norm = [];
for bx=1:length(Bx)
    Feat = [Feat; Bx{bx}.Feat];
    L_BX = [L_BX; ones(size(Bx{bx}.Feat,1),1)*bx];

end

%% normalization by inferring bg signal
for i=1:length(Bx{1}.FeatName)
    id = []; id = strfind(Bx{1}.FeatName{i}, '-');
    marker_name{i} = Bx{1}.FeatName{i}(1:id(1)-1);
end




marker_pairs_ref = {'CD4', 'Ecad', 'PD1', 'CK7', 'GRNZB', ...
                    'CK17', 'PDPN', 'CK19', 'HER2', 'FoxP3', ...
                    'PgR', 'ER', 'CD3', ...
                    'CD45', 'aSMA', ...
                    'VIM', 'CK14', 'CK5', 'CD8', 'CD31', 'CD20', ...
                    'EGFR', 'CK8', 'FoxP3bio', 'ColI', 'ColIV', 'PCNA' , 'CD44', ...
                    'pHH3', 'LamAC',  ... 
                    };
                

marker_pairs_tgr = {'CK19', 'CD45', 'CK19', 'CD8', 'CK19', ...
                    'CD31', 'CK19', 'CD31', 'CD31', 'CK19', ...
                    'CD31', 'CD45', 'CK19', ...
                    'CK19', 'CK19',  ...
                    'CK7', 'CD31', 'CD31', 'CK19', 'CD45', 'CD45', ...
                    'CD31', 'CD31', 'Ck19', 'CK19', 'CK19', 'CD31',  'CK19', ...
                    'CK19', 'CK19',  ...
                    };
                
marker_pairs = [];
for i=1:length(marker_name)
    id_ref = []; id_ref = find(strcmp(marker_pairs_ref, marker_name{i})==1);
    if isempty(id_ref) ~= 1
        id_tgr = [];
        id_tgr = find(strcmp(marker_name, marker_pairs_tgr{id_ref}));
        marker_pairs = [marker_pairs; [i id_tgr]];
    end
    
end

[marker_name(marker_pairs(:,1)); marker_name(marker_pairs(:,2))]

%
if FLAG_INFER == 1

    th_mInt = 50;
    ratio_x = 0.7;
    ratio_y = 0.7;


    mIntData = Feat;
    L_smpl = L_BX;

    Thresh_all = [ ];
    for i=1:size(marker_pairs,1)
        N_cluster = 2;

        fprintf('%s ==============\n', marker_name{marker_pairs(i,1)});


        % local estimate
        Thresh_Est = zeros(length(data), 2);
        N_comp = [];

        for batch=1:max(L_smpl)
            id = []; id = find(L_smpl == batch);
            id_select = [];
            id_select = find ( (( mIntData(id, marker_pairs(i,2)) > th_mInt ) & ...
                                mIntData(id, marker_pairs(i,1)) > quantile( mIntData(id, marker_pairs(i,1)), ratio_x)) | ...
                               (( mIntData(id, marker_pairs(i,1)) > th_mInt) & ...
                               mIntData(id, marker_pairs(i,2)) > quantile( mIntData(id, marker_pairs(i,2)), ratio_y) ));

            Feat_ij = [  mIntData(id(id_select), marker_pairs(i,1)) mIntData(id(id_select), marker_pairs(i,2))];

            if FLAG_SSC == 1
                Z_kssc = kssc_exact_par(Feat_ij', 0.1, 500);
                [kssc_clusters,NcutEigenvectors,NcutEigenvalues] = ncutW((abs(Z_kssc)+abs(Z_kssc')), N_cluster);

                L = condense_clusters(kssc_clusters,1);
            else
                L = litekmeans(Feat_ij, N_cluster);
            end

            mean_value = [mean( Feat_ij(find(L==1), 1)) mean(Feat_ij(find(L==2),1))];

            if mean_value(1) > mean_value(2)
                flag_neg = 2;
            else
                flag_neg = 1; 
            end

            thresh_est = (min(mean(Feat_ij( find(L==flag_neg),1)) + 3*std(Feat_ij( find(L==flag_neg),1)), ...
                            quantile(Feat_ij( find(L==flag_neg), 1), 0.95))*1.10); 

            Thresh_Est(batch,:)  = [thresh_est]; % thresh_global_est];



        end
        Thresh_all = [Thresh_all; Thresh_Est(:,1)' ]; %Thresh_Est(1,2)'];


    end

    Feat_norm = Feat;

    for bx=1:max(L_BX)
        id_smpl = []; id_smpl = find(L_BX == bx);
        for l=1:size(marker_pairs,1)
         
            Feat_norm(id_smpl,l) = Feat_norm(id_smpl, l) ./ (Thresh_all(l, bx)) * mean(Thresh_all(l,1:2));
        end

    end

    save(sprintf('%sthreshold_info.mat',output_path), 'Thresh_all', 'L_BX', 'Feat_norm', 'Feat');
else
    load(sprintf('%sthreshold_info.mat',output_path));
end

%

figure
for i=1:size(Bx{1}.FeatName,2)
    id1 = find(L_BX == 1);
    id2 = find(L_BX == 2);
    [n1,x1] = hist(log10(Feat(id1,i)),200);
    [n2,x2] = hist(log10(Feat(id2,i)),200);
    
    n1 = n1/sum(n1); n2 = n2/sum(n2);
    
    subplot(4,5,i); 
    
    plot(x1,n1,x2,n2, 'LineWidth',3);
    hold on;
    
    if i==1
        legend({'Bx1', 'Bx2'});
    end
    title(Bx{1}.FeatName{i});
    xlabel('mean intensity (log scale)');
    ylabel('ratio'); 
end
saveas(gcf, sprintf('%shist_meanInt_dist_bx_wo_norm.fig', output_path));

figure
for i=1:size(Bx{1}.FeatName,2)
    id1 = find(L_BX == 1);
    id2 = find(L_BX == 2);
    [n1,x1] = hist(log10(Feat_norm(id1,i)),200);
    [n2,x2] = hist(log10(Feat_norm(id2,i)),200);
    
    n1 = n1/sum(n1); n2 = n2/sum(n2);
    
    subplot(4,5,i); 
    
    plot(x1,n1,x2,n2, 'LineWidth',3);
    
    if i==1
        legend({'Bx1', 'Bx2'});
    end
    title(Bx{1}.FeatName{i});
    xlabel('mean intensity (log scale)');
    ylabel('ratio'); 
end
saveas(gcf, sprintf('%shist_meanInt_dist_bx_w_norm.fig', output_path));



figure; 
imagesc(log10([Feat Feat_norm]));
colormap redbluecmap;


saveas(gcf, sprintf('%sheatmap.fig', output_path));

