import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="example-pkg-YOUR-USERNAME-HERE", # Replace with your own username
    version="0.0.1",
    author="Erik Burlingame",
    author_email="burlinge@ohsu.edu",
    description="RESTORE",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/Chang_Lab/cycif_int_norm",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)