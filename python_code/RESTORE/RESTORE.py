import os
import pickle
import numpy as np
import pandas as pd
from glob import glob
import holoviews as hv
from selenium import webdriver
from tqdm.notebook import tqdm
from collections import defaultdict
from multiprocessing.dummy import Pool

from sklearn import cluster
from sklearn import mixture
from scipy.stats import gaussian_kde
from ssc.cluster import selfrepresentation as sr

def nested_dict():
    """
    A nested dictionary for hierarchical storage of thresholds.
    """
    return defaultdict(nested_dict)


class Normalization:
    """
    Automated intensity normalization framework for cycIF imaging datasets.
    
    Parameters
    ----------
    
    data: pandas DataFrame
        Contains batch ids, scene ids, cell ids, and marker intensities.
    marker_pairs: list of lists
        Each sub-list contains a marker and its exclusive counterpart marker.
    save_dir: string
        Path to directory for saving results
    manual_thresh: nested dict, optional
        Manual thresholds for adding to figures
        
    Attributes
    ----------
    threshs: nested dictionary
    
    """
    
    def __init__(self, data, marker_pairs, save_dir, save_figs=True, manual_threshs=None):
        self.data = data
        self.threshs = nested_dict()
        self.marker_pairs = marker_pairs
        self.save_dir = save_dir
        self.save_figs = save_figs
        self.manual_threshs = manual_threshs
        
        
    def get_thresh_curve(self, data, marker_pair, thresh, color, batch):
        """
        Get holoview curve from thresh
        
        Parameters
        ----------
        data: pandas Dataframe
            Contains batch ids, scene ids, cell ids, and marker intensities.
        marker_pair: list
            A two-element list of marker names
        thresh: int (?)
            
        """

        p = int(round(data[:,1].max()))
        x = np.array([thresh] * p)[:,np.newaxis]
        y = np.arange(0, p)[:,np.newaxis]

        xlabel = marker_pair[0]
        ylabel = marker_pair[1]

        xmax = np.quantile(data[:,0],0.999)
        xlim = (0, max(thresh*1.1, xmax))

        ymax = np.quantile(data[:,1],0.999)
        ylim = (0, ymax)
        
        if batch == 'global':
            line_dash = 'solid'
        else:
            line_dash='dotted'
            
        curve = hv.Curve(np.hstack((x,y))).opts(xlabel=xlabel, 
                                                ylabel=ylabel,
                                                xrotation=45,
                                                xlim=xlim,
                                                ylim=ylim,
                                                tools=['hover'],
                                                show_legend=False,
                                                line_width=2,
                                                color=color,
                                                line_dash='solid')
        return curve
        
        
    def get_GMM_thresh(self, data, marker_pair, model, sigma_weight, color, batch):
        """
        sigma_weight: int, Weighting factor for sigma, where higher value == fewer cells classified as positive.
        """

        model.fit(data)

        neg_idx = np.argmax([np.diagonal(i)[1] for i in model.covariances_])

        mu = model.means_[neg_idx,0]

        # Extract sigma from covariance matrix
        sigma = np.sqrt(np.diagonal(model.covariances_[neg_idx])[0])

        thresh = mu + sigma_weight * sigma

        curve = self.get_thresh_curve(data, marker_pair, thresh, color, batch)

        return thresh, curve


    def get_clustering_thresh(self, data, marker_pair, model, sigma_weight, color, batch):
        """
        sigma_weight: int, Weighting factor for sigma, where higher value == fewer cells classified as positive.
        """

        model.fit(data)

        clusters = [
            data[model.labels_.astype('bool')], 
            data[~model.labels_.astype('bool')]
        ]

        # Identify negative cluster based on maximum std on y-axis
        neg_cluster = clusters[np.argmax([i[:,1].std() for i in clusters])]

        mu  = neg_cluster[:,0].mean()
        sigma = neg_cluster[:,0].std()

        thresh = mu + sigma_weight * sigma

        curve = self.get_thresh_curve(data, marker_pair, thresh, color, batch)

        return thresh, curve
    

    def get_marker_pair_thresh(self, data, scene, marker_pair, batch):
        """
        Docs
        """
        ratio_x = 0.75
        ratio_y = 0.5
        
        tmp = data[marker_pair].to_numpy()
        
        #TODO: parameterize thresh
        idx_select = (
            (
                (tmp[:,0] > 50) * (tmp[:,1] > np.quantile(tmp[:,1],ratio_y))
            ) + (
                (tmp[:,1] > 50) * (tmp[:,0] > np.quantile(tmp[:,0],ratio_x))
            )
        )
        
        marker_pair_data = tmp[idx_select] 
        
        xlabel = marker_pair[0]
        ylabel = marker_pair[1]

        models = (
            ('KMeans', 'magenta', cluster.KMeans(n_clusters=2)),
            ('GMM',    'blue',  mixture.GaussianMixture(n_components=2, n_init=10)),
            ('SSC',    'green', sr.SparseSubspaceClusteringOMP(n_clusters=2))
        )

        sigma_weight = 3 #TODO: parameterize
        
        curves = []
        
        for name, color, model in models:

            if name == 'GMM':

                thresh, curve = self.get_GMM_thresh(marker_pair_data, 
                                                    marker_pair, 
                                                    model, 
                                                    sigma_weight,
                                                    color,
                                                    batch)

            else:

                thresh, curve = self.get_clustering_thresh(marker_pair_data, 
                                                           marker_pair, 
                                                           model, 
                                                           sigma_weight,
                                                           color,
                                                           batch)
                
            curves.append(curve)

            self.threshs[batch][scene][xlabel][name] = thresh
            
        if self.manual_threshs != None and batch != 'global':
            manual_thresh = self.manual_threshs[batch][scene][xlabel]
            manual_curve  = hv.VLine(manual_thresh).opts(line_dash=(6,6), 
                                                         line_width=2, 
                                                         color='black')
            curves.append(manual_curve)
                
        if batch == 'global':
            scatter = hv.Scatter(marker_pair_data).opts(xlabel=xlabel.split('_')[0], 
                                                        ylabel=ylabel.split('_')[0],
                                                        xrotation=45,
                                                        show_legend=False,
                                                        alpha=0.2,
                                                        color='gray')
        else:
            
            # Get kde for plotting density of local scatter plot
            marker_pair_data_T = marker_pair_data.T
            z = gaussian_kde(marker_pair_data_T)(marker_pair_data_T)
            marker_pair_data = np.vstack((marker_pair_data_T, z)).T
            
            scatter = hv.Scatter(marker_pair_data,vdims=['y','z']).opts(xlabel=xlabel.split('_')[0], 
                                                                        ylabel=ylabel.split('_')[0],
                                                                        xrotation=45,
                                                                        show_legend=False,
                                                                        color='z')        
        return scatter, curves
    
    
    def save_thresh_figs(self, scene_scatters, scene, save_dir):
        """
        Save hv figures for all marker pairs for a given scene as an hv.Layout()
        """
        
        final_fig = hv.Layout(scene_scatters).opts(title=scene,shared_axes=False).cols(3)
        hv.save(final_fig, f'{save_dir}/{scene}_gate_dist.html')

        driver = webdriver.PhantomJS()
        driver.maximize_window()
        driver.get(f'{save_dir}/{scene}_gate_dist.html')
        driver.save_screenshot(f'{save_dir}/{scene}_gate_dist.png')
        driver.quit()
    
    
    def normalize_scene(self, scene):
        """
        Normalization and figure generation
        """

        scene_data = self.data[self.data.scene == scene]

        scene_scatters = []

        for marker_pair in tqdm(self.marker_pairs, desc=f'Processing {scene}'):

            global_scatter, _ = self.get_marker_pair_thresh(scene_data,
                                                            scene,
                                                            marker_pair, 
                                                            'global')          
            
            for batch in set(scene_data.batch):

                batch_scene_data = scene_data[scene_data.batch == batch]

                local_scatter, local_curves = self.get_marker_pair_thresh(batch_scene_data, 
                                                                          scene,
                                                                          marker_pair, 
                                                                          batch)

                scene_scatters.append(global_scatter * local_scatter * hv.Overlay(local_curves))
                
        if self.save_figs:
            self.save_thresh_figs(scene_scatters, scene, self.save_dir)
        
    
    def run(self):
        
        os.makedirs(self.save_dir, exist_ok=True)

        with Pool() as p:
            p.map(self.normalize_scene, set(self.data.scene))
            p.close()
            p.join()
            
        pickle.dump(self.threshs, open(f'{self.save_dir}/threshs.pkl', 'wb'))